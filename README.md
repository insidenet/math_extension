# JavaScript Mathオブジェクト拡張ライブラリ
JavaScriptは現在[IEEE754](https://ja.wikipedia.org/wiki/IEEE_754)規格に従い、浮動小数点演算が実装されている。  
そのため、乗算・減算時に誤差が生じる場合が存在する。  
本ライブラリは、その誤差をなくし、正しい値を得るために作成された。  

## 機能一覧

* [Math拡張 指数形式にも対応した数値to文字列変換関数](https://inside-research.com/3522/javascript-%e6%8c%87%e6%95%b0%e5%bd%a2%e5%bc%8f%e3%81%ab%e3%82%82%e5%af%be%e5%bf%9c%e3%81%97%e3%81%9f%e6%95%b0%e5%80%a4to%e6%96%87%e5%ad%97%e5%88%97%e5%a4%89%e6%8f%9b%e9%96%a2%e6%95%b0)
* [Math拡張 減算関数](https://inside-research.com/3872/javascript-%E6%B8%9B%E7%AE%97%E9%96%A2%E6%95%B0)
* [Math拡張 乗算関数](https://inside-research.com/4169/javascript-math%E6%8B%A1%E5%BC%B5-%E4%B9%97%E7%AE%97%E9%96%A2%E6%95%B0)
* [Math拡張 指数形式浮動小数点チェック関数](https://inside-research.com/4155/javascript-math%E6%8B%A1%E5%BC%B5-%E6%8C%87%E6%95%B0%E5%BD%A2%E5%BC%8F%E6%B5%AE%E5%8B%95%E5%B0%8F%E6%95%B0%E7%82%B9%E3%83%81%E3%82%A7%E3%83%83%E3%82%AF%E9%96%A2%E6%95%B0)

- - -
## 更新履歴

##### 2017/02/09
* 指数形式浮動小数点チェック関数を追加
* 乗算関数を追加

##### 2017/02/08
* 減算関数を追加

##### 2017/02/07
* 新規作成

##### 2017/02/22
* Gitアップミスを訂正
* テスト・プログラム（test.html）を追加
